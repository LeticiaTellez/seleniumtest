﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using UCA.WebCalculator.Api;

namespace UCA.WebCalculator.Tests
{
    [TestClass]
    public class CalculatorFixture
    {
        private IWebDriver driver;
        public string homeURL;

        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }
        
        private Calculator _SystemUnderTest;
        public Calculator SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }

                return _SystemUnderTest;
            }
        }

        [TestMethod]
        public void Add()
        {
            // arrange
            double value1 = 2;
            double value2 = 3;
            double expected = 5;

            // act
            double actual = SystemUnderTest.Add(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }


       


        [TestMethod]
        public void Subtract()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 4;

            // act
            double actual = SystemUnderTest.Subtract(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Multiply()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 12;

            // act
            double actual = SystemUnderTest.Multiply(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Divide()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 3;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivideByZeroThrowsException()
        {
            // arrange
            double value1 = 6;
            double value2 = 0;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);
        }

        [TestMethod]
        public void TestSide()
        {
            //cargamos las variables
            driver = new ChromeDriver(@"C:\Users\Leticia Téllez\Desktop\UCA\");
            homeURL = "https://localhost:44348/Calculator";

            //instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.Name("Value1")).Clear();
            driver.FindElement(By.Name("Value1")).SendKeys("1");
            var a = driver.FindElement(By.Name("Operator"));
            var selectElementA = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementA.SelectByValue("Add");

            driver.FindElement(By.Name("Value2")).Clear();
            driver.FindElement(By.Name("Value2")).SendKeys("1");

            //Click al botón calcular
            driver.FindElement(By.Id("btnCalculate")).Click();

            //Cerramos el driver
            driver.Close();
        }

        [TestMethod]
        public void TestSubtract()
        {
            //cargamos las variables
            driver = new ChromeDriver(@"C:\Users\Leticia Téllez\Desktop\UCA\");
            homeURL = "https://localhost:44348/Calculator";

            //instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.Name("Value1")).Clear();
            driver.FindElement(By.Name("Value1")).SendKeys("34");
            var a = driver.FindElement(By.Name("Operator"));
            var selectElementA = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementA.SelectByValue("Subtract");

            driver.FindElement(By.Name("Value2")).Clear();
            driver.FindElement(By.Name("Value2")).SendKeys("15");

            //Click al botón calcular
            driver.FindElement(By.Id("btnCalculate")).Click();

            //Cerramos el driver
            driver.Close();
        }

        [TestMethod]
        public void TestMultiply()
        {
            //cargamos las variables
            driver = new ChromeDriver(@"C:\Users\Leticia Téllez\Desktop\UCA\");
            homeURL = "https://localhost:44348/Calculator";

            //instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.Name("Value1")).Clear();
            driver.FindElement(By.Name("Value1")).SendKeys("12");
            var a = driver.FindElement(By.Name("Operator"));
            var selectElementA = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementA.SelectByValue("Multiply");

            driver.FindElement(By.Name("Value2")).Clear();
            driver.FindElement(By.Name("Value2")).SendKeys("6");

            //Click al botón calcular
            driver.FindElement(By.Id("btnCalculate")).Click();

            //Cerramos el driver
            driver.Close();
        }

        [TestMethod]
        public void TestDivide()
        {
            //cargamos las variables
            driver = new ChromeDriver(@"C:\Users\Leticia Téllez\Desktop\UCA\");
            homeURL = "https://localhost:44348/Calculator";

            //instanciamos los objetos
            driver.Navigate().GoToUrl(homeURL);

            //Accesamos a las propiedades del objeto a manipular
            driver.FindElement(By.Name("Value1")).Clear();
            driver.FindElement(By.Name("Value1")).SendKeys("186");
            var a = driver.FindElement(By.Name("Operator"));
            var selectElementA = new OpenQA.Selenium.Support.UI.SelectElement(a);

            //select by value
            selectElementA.SelectByValue("Divide");

            driver.FindElement(By.Name("Value2")).Clear();
            driver.FindElement(By.Name("Value2")).SendKeys("6");

            //Click al botón calcular
            driver.FindElement(By.Id("btnCalculate")).Click();

            //Cerramos el driver
            driver.Close();
        }
    }
}
