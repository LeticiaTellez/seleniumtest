# Pruebas Unitarias - Agenda 8
***Pruebas Unitarias con Selenium***

Este proyecto se basa en la guía otorgada para la `Agenda 8`, donde se desarrollan tres métodos de pruebas con las operaciones matemáticas básicas (Resta, Multiplicación y División).

Dichas pruebas consisten en garantizar la funcionalidad de las operaciones matemáticas listadas en el combo `Operator` haciendo uso de Selenium, una herramienta para automatizar las pruebas realizadas en los navegadores web.

**Integrantes**
 - Yosselin Peña Contreras.
 - Amara López Galo.
 - Leticia Téllez Fargas.
 - Brian Téllez Hernández.

